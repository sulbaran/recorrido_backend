Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post '/sheets/avaliable', to: 'sheets#index'
  get '/sheets/current_week_more_five', to: 'sheets#current_week_more_five'
  post '/sheets/add_hours', to: 'sheets#add_hours'
  post '/sheets/confirmed', to: 'sheets#confirmed'
  post '/api/login', to: 'api#login'
end
