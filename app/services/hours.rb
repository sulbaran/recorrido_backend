# frozen_string_literal: true

# clase para asignar horas a usuario
class Hours
  def self.assign_hours(turns)
    hours_by_day = {}
    data = turns.data

    @days = []
    create_days_data(data, hours_by_day)
    top_by_days = []
    7.times do |t|
      top_by_days << create_top_day(t + 1)
    end
    create_hours_confirmed(turns, turns.data, top_by_days)
  end

  def self.create_top_day(day_id)
    { day: day_id, user_id: @days.select { |a| a[:id] == day_id and a[:amount].positive? }
                                 .sort_by { |b| b[:amount] }.reverse.first }[:user_id]
  end

  def self.create_days_data(data, hours_by_day)
    users = User.all.order(:id).pluck(:id)
    users.each do |user_id|
      data.each do |dat|
        hours_by_day = { id: dat['id'], user_id: user_id, amount: 0 }
        dat['turns'].each do |turn|
          hours_by_day[:amount] += 1 if available_user(turn, user_id)
        end
        @days << hours_by_day
      end
    end
  end

  def self.available_user(turn, user_id)
    turn['users'].find { |a| a['id'] == user_id && a['available'] }
  end

  def self.create_hours_confirmed(turns, data, top_by_days)
    data_turns = new_data(data, top_by_days)
    total_by_users = top_by_days.compact.group_by { |a| a[:user_id] }
                                .map { |b, c| { user_id: b, total: c.sum { |d| d[:amount] } } }
    confirmed = {
      total_by_users: total_by_users,
      data: data_turns
    }
    Turn.find(turns.id).update(hours_confirmed: confirmed)
  end

  def self.new_data(original_data, top_by_days)
    original_data.each do |original|
      user_id = top_by_days.compact.find { |a| a[:id] == original['id'] }
      user_id = user_id.present? ? user_id[:user_id] : 0
      original['turns'] = turn_filter(original_data, original, user_id)
    end
  end

  def self.turn_filter(turns, original, user_id)
    turns.select { |a| a['id'] == original['id'] }.first['turns']
         .map do |b|
           {
             'turn' => b['turn'],
             'user' => clear_user(b, user_id)
           }
         end
  end

  def self.clear_user(user, user_id)
    id = user['users'].find { |c| c['id'] == user_id and c['available'] }
    id.present? ? id['id'] : 0
  end
end
