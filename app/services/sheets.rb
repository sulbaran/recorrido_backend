# frozen_string_literal: true

# clase para calcular horas disponibles
class Sheets
  DAYS_WEEK = %w[Lunes Martes Miercoles Jueves Viernes Sabado Domingo].freeze
  MONTHS = %w[Enero Febrero Marzo Abril Mayo Junio Julio Agosto Septiembre octubre Noviembre Diciembre].freeze

  def self.times_for_week(week_number, client_id)
    @client_times = extract_client_contract_times(client_id)
    start_day = Date.commercial(Time.now.year, week_number, 1)
    end_day = Date.commercial(Time.now.year, week_number, 7)
    generate_grid(start_day, end_day)
  end

  def self.generate_grid(start_day, end_day)
    turns = []
    days = start_day..end_day
    users = User.select('id, false as available').active.to_a
    days.each_with_index do |day, index|
      turns << create_day_data(day, index, users)
    end
    turns
  end

  def self.create_day_data(day, index, users)
    start_time, end_time = contract_hours(index)
    {
      id: index + 1,
      text: "#{DAYS_WEEK[index]} #{make_pretty_day(day)}",
      turns: block_hours(start_time, end_time, day).map { |hour| { turn: pretty_hours(hour), users: users } }
    }
  end

  def self.extract_client_contract_times(client_id)
    Agreement.find_by(client_id: client_id).times['days']
  end

  def self.make_pretty_day(datetime)
    "#{datetime.day.to_s.rjust(2, '0')} de #{MONTHS[datetime.month - 1]}"
  end

  def self.contract_hours(day)
    find_times = @client_times.find { |data| data['day'].include?(day) }
    [find_times['start_time'], find_times['end_time']]
  end

  def self.make_date(time, day)
    hours, minutes = time.split(':')
    (day.to_time + hours.to_i.hours + minutes.to_i.minutes).to_datetime
  end

  def self.block_hours(start_time, end_time, day)
    start_time = make_date(start_time, day)
    end_time = make_date(end_time, day)
    hour_step = (1.to_f / 24)
    start_time.step(end_time, hour_step)
  end

  def self.pretty_hours(time)
    "#{time.strftime('%H:%M')}-#{(time + 1.hour).strftime('%H:%M')}"
  end
end
