class Agreement < ApplicationRecord
  belongs_to :client
  has_many :turn

  enum status: %i[active inactive]
end
