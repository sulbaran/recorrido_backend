class User < ApplicationRecord
  enum status: %i[active inactive]
end
