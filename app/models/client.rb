class Client < ApplicationRecord
  has_one :agreement

  enum status: %i[active inactive suspended]
end
