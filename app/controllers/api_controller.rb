# frozen_string_literal: true

# Api controller
class ApiController < ActionController::API
  def login
    current_user = User.find_by(email: params[:email], password: params[:password])
    return render json: { msg: 'Error de correo o clave' }, status: 400 unless current_user.present?

    current_user.update(token: SecureRandom.base58)
    render json: { msg: 'OK', user_id: current_user.id }
  end
end
