# frozen_string_literal: true

# Sheets controller
class SheetsController < ActionController::API
  def current_week_more_five
    create_data_query
  rescue Date::Error => _e
    # consiguio una fecha invalida pero entrega las disponibles
    Rails.logger.error 'Fecha invalida'
  ensure
    render json: { weeks: @weeks, clients: clients, users: users }
  end

  def clients
    Client.select(:id, :name).all.order(:name)
  end

  def index
    client_id, week_number, year = set_params
    return invalid_params unless valid_request

    agreement = Client.find(client_id).agreement
    turns = agreement.turn.find_by(week: week_number, year: year)

    turn_data(turns, client_id, agreement, week_number, year)
  end

  def add_hours
    client = Client.find_by(id: params[:client_id])
    render json: { msg: 'Cliente no existe' }, status: 400 and return unless client.present?

    turns = client.agreement.turn.find_by(week: params[:week], year: params[:year])
    update_turns(turns)
    Hours.assign_hours(turns)
    render json: { msg: 'Completado' }
  end

  def confirmed
    client_id, week_number, year = set_params
    return invalid_params unless valid_request

    agreement = Client.find(client_id).agreement
    turn = agreement.turn.find_by(week: week_number, year: year)
    return render json: { msg: 'No hay horas confirmadas' }, status: 400 unless turn.hours_confirmed.present?

    render json: { data: turn.hours_confirmed, week_number: week_number, year: year }
  end

  private

  def set_params
    [
      params[:client_id].to_i,
      params[:week_number].to_i,
      params[:year]
    ]
  end

  def update_turns(turns)
    turns.update(data: params[:turn])
  end

  def create_data_query
    current = Time.now.strftime('%W').to_i
    year = Time.now.year
    @weeks = []
    6.times do |time|
      current_week = current + time
      range_days = "#{Date.commercial(year, current + time, 1).strftime('%d/%m/%Y')} - "\
                   "#{Date.commercial(year, current + time, 7).strftime('%d/%m/%Y')}"
      @weeks << { week: current_week, year: year, days: range_days }
    end
  end

  def users
    User.select('id, name, 0 as amount').all.order(:id)
  end

  def turn_data(turns, client_id, agreement, week_number, year)
    turn = if turns.present?
             turns.data
           else
             create_turn(client_id, agreement, week_number)
           end
    render json: { data: turn, week_number: week_number, year: year }
  end

  def invalid_params
    render json: { msg: 'Verifique los parametros enviados' }, status: 400
  end

  def valid_request
    params[:client_id].present? && params[:week_number].present? && params[:year].present?
  end

  def create_turn(client_id, agreement, week_number)
    data = Sheets.times_for_week(week_number, client_id)
    agreement.turn.find_or_create_by(week: week_number, year: Time.now.year, data: data)
    data
  end
end
