# Recorrido
## Desafío Técnico

## Instalación

Require [Ruby 3.0.0]

Instalar dependencias

```sh
cd recorrido_backend
bundle install
```
Crear usuario en postgresql

```sh
sudo su - postgres
psql
CREATE ROLE recorrido WITH SUPERUSER CREATEDB CREATEROLE LOGIN PASSWORD '123456';
```

Inicializar Base de datos

```sh
rake db:setup
rake db:migrate
rake db:seed
```
Para ejecutar el servidor

```sh
rails s -p3000 -b 0.0.0.0
```