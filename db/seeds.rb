# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: 'barbara@gmail.com', name: 'Barbara', last_name: 'Perez', password: '123456', status: 'active')
User.create(email: 'ernesto@gmail.com', name: 'Ernesto', last_name: 'Perez', password: '123456', status: 'active')
User.create(email: 'benja@gmail.com', name: 'Benjamin', last_name: 'Perez', password: '123456', status: 'active')

client = Client.create(name: 'Recorrido', dni: '123', email: 'recorrido@gmail.com', manager: 'Pedro', status: 'active')
times = {
  days: [
    {
      day: [0, 1, 2, 3, 4],
      start_time: '17:00',
      end_time: '23:00'
    },
    {
      day: [5, 6],
      start_time: '10:00',
      end_time: '23:00'
    }
  ]
}
client.create_agreement(times: times, status: 'active')


