class CreateClients < ActiveRecord::Migration[6.1]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :dni
      t.string :email
      t.string :manager
      t.integer :status

      t.timestamps
    end
  end
end
