class CreateTurns < ActiveRecord::Migration[6.1]
  def change
    create_table :turns do |t|
      t.references :agreement, null: false, foreign_key: true
      t.jsonb :data, default: []
      t.jsonb :hours_confirmed, default: []
      t.integer :week
      t.integer :year

      t.timestamps
    end
  end
end
