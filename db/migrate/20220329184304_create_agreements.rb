class CreateAgreements < ActiveRecord::Migration[6.1]
  def change
    create_table :agreements do |t|
      t.references :client, null: false, foreign_key: true
      t.jsonb :times
      t.integer :status

      t.timestamps
    end
  end
end
